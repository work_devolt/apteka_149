@extends('layouts.app')

@section('content')
    @include('components.navbar.navbar_white')
<div class="container section-pad-top section-pad-bottom mx-auto">
    <div class="row justify-content-center">

                    <form method="POST" action="{{ route('login') }}">
                        @csrf




                    <!-- component -->
                        <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 flex flex-col w-full lg:w-3/5 mx-auto">
                            <div class="mb-4">
                                <label class="block text-grey-darker text-sm font-bold mb-2" for="email">
                                    Username
                                </label>
                                <input class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker" id="email" type="text" name="email" placeholder="Username">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-6">
                                <label class="block text-grey-darker text-sm font-bold mb-2" for="password">
                                    Password
                                </label>
                                <input class="shadow appearance-none border border-red rounded w-full py-2 px-3 text-grey-darker mb-3" id="password" name="password" type="password" placeholder="******************">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="flex items-center justify-between">
                                <button class="bg-primary text-white font-bold py-2 px-4 rounded" type="submit">
                                    Sign In
                                </button>
                                <a class="inline-block align-baseline font-bold text-sm text-blue hover:text-blue-darker" href="#">
                                    Forgot Password?
                                </a>
                            </div>
                        </div>

                    </form>
    </div>
</div>
@endsection
