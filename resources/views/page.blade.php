@extends('layouts.app', [
    'title' => $page->title,
    'description' => $page->description,
    'keywords' => $page->keywords
])
@section('content')
    @include('components.navbar.navbar_white')

    <section class="container mx-auto section-pad-bottom section-pad-top page">

        <div class="mt-6 mx-auto w-full lg:w-2/3 xl:w-4/5 text-lg">
            {!! $page->content !!}
        </div>
    </section>



@endsection


