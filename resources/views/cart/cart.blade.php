@extends('layouts.app', [
    'title' => 'Корзина'
])
@section('content')
    @include('components.navbar.navbar_white')

    <!-- component -->
    @if( \Cart::isEmpty() )
    <div class="flex justify-center mt-32 mb-24">
        <h1 class="mb-6">Ваша корзина пуста, посмотрите наш <a href=" {{route('categories.index')}} "><span class="text-primary underline">каталог</span></a></h1>
    </div>
    @else
    <div class="flex justify-center my-6 mb-24">

        <div class="flex flex-col w-full p-8 text-gray-800 bg-white shadow-lg pin-r pin-y md:w-4/5 lg:w-4/5">

            <h1 class="mb-6 text-primary">Корзина</h1>

            <div class="flex-1">
                <table class="w-full text-sm lg:text-base" cellspacing="0">
                    <thead>
                    <tr class="h-12 uppercase">
                        <th class="text-left">Продукт</th>
                        <th class="lg:text-right text-left pl-5 lg:pl-0">
                            <span class="lg:hidden" title="Quantity">Кол-во</span>
                            <span class="hidden lg:inline">Количество</span>
                        </th>
                        <th class="hidden text-right md:table-cell">Цена</th>
                        <th class="text-right">Сумма</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cart = Cart::getContent() as $cartItem)
                        <tr>
                            <td>
                                <a href="{{ route('products.show', $cartItem->id) }}"><p
                                        class="mb-2 md:ml-4">{{ $cartItem->name }}</p></a>
                                <form action="{{ route('cart.delete', $cartItem->id) }}" method="GET">
                                    <button type="submit" class="text-gray-700 md:ml-4">
                                        <small class="text-primary">(Удалить товар)</small>
                                    </button>
                                </form>

                            </td>
                            <td class="w-24">
                                {{ $cartItem -> qty }}
                                <form action="{{route('cart.add', $cartItem->id)}}">
                                    <input type="number" value="{{ $cartItem->quantity }}" name="quantity"
                                           class="w-full font-semibold text-center text-gray-700 bg-gray-200 outline-none focus:outline-none hover:text-black focus:text-black mb-2"/>
                                    <button type="submit" class="text-gray-700">
                                        <small class="text-primary">(Обновить)</small>
                                    </button>
                                </form>

                            </td>
                            <td class="hidden text-right md:table-cell">
              <span class="text-sm lg:text-base font-medium">
                {{ $cartItem->price }} ₽
              </span>
                            </td>
                            <td class="text-right">
              <span class="text-sm lg:text-base font-medium">
                {{ Cart::get($cartItem->id)->getPriceSum() }} ₽
              </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <hr class="pb-6 mt-6">
                <div class="my-4 mt-6 -mx-2 lg:flex">
                    <div class="lg:px-2 lg:w-full">

                        <h1 class="text-primary mb-6">Детали заказа</h1>
                        <div class="p-4">
                            @if(env('DELIVERY'))
                                <div class="flex justify-between border-b">
                                    <div class="flex-col lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-left text-gray-800">
                                        <div>Доставка</div>
                                        <small class="text-primary font-normal">Вы можете заказать доставку или оформить самовывоз</small>
                                    </div>
                                    <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                        @if( \Cart::getSubTotal() >= 500 ) Бесплатная доставка @else {{ env( 'DELIVERY_PRICE' ) }} ₽ @endif
                                    </div>
                                </div>
                                <div class="flex justify-between border-b">
                                    <div class="flex-col lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-left text-gray-800">
                                        <div>Итого</div>
                                        <small class="text-primary font-normal">Цена без учета доставки</small>
                                    </div>
                                    <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                        {{ \Cart::getSubTotal() }} ₽
                                    </div>
                                </div>
                            @else
                                <div class="flex justify-between border-b">
                                    <div class="flex-col lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-left text-gray-800">
                                        <div>Самовывоз</div>
                                        <small class="text-primary font-normal">Вы можете оформить самовывоз из наших аптек</small>
                                    </div>
                                    <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                        Бесплатно
                                    </div>
                                </div>
                                <div class="flex justify-between border-b">
                                    <div class="flex-col lg:px-4 lg:py-2 m-2 text-lg lg:text-xl font-bold text-left text-gray-800">
                                        <div>Итого</div>
                                    </div>
                                    <div class="lg:px-4 lg:py-2 m-2 lg:text-lg font-bold text-center text-gray-900">
                                        {{ \Cart::getSubTotal() }} ₽
                                    </div>
                                </div>
                            @endif


                                <div class="lg:px-4 lg:py-2 m-2 text-lg lg:text-xl">
                                    <small class="text-primary font-normal">
                                        Финальная цена может отличаться в зависимости от партии.
                                    </small>
                                </div>

                                <a href="{{ route('checkout.index') }}">
                                <button
                                    class="flex justify-center w-full px-10 py-3 mt-6 font-medium text-primary uppercase bg-white text-white border border-primary rounded-full shadow item-center hover:bg-primary hover:text-white focus:shadow-outline focus:outline-none">
                                    <svg aria-hidden="true" data-prefix="far" data-icon="credit-card" class="w-8"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                        <path fill="currentColor"
                                              d="M527.9 32H48.1C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48.1 48h479.8c26.6 0 48.1-21.5 48.1-48V80c0-26.5-21.5-48-48.1-48zM54.1 80h467.8c3.3 0 6 2.7 6 6v42H48.1V86c0-3.3 2.7-6 6-6zm467.8 352H54.1c-3.3 0-6-2.7-6-6V256h479.8v170c0 3.3-2.7 6-6 6zM192 332v40c0 6.6-5.4 12-12 12h-72c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12zm192 0v40c0 6.6-5.4 12-12 12H236c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12z"/>
                                    </svg>
                                    <span class="ml-2 mt-5px">Перейти к оформлению</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection
