@extends('layouts.app', [
    'title' => 'Оформление заказа'
])
@section('content')
    @include('components.navbar.navbar_white')


    <!-- component -->
    <div class="flex justify-center mt-24 mb-40">

        <div class="flex flex-col w-full p-2 md:p-8 text-gray-800 bg-white shadow-lg pin-r pin-y lg:w-4/5">
            @if( \Cart::isEmpty() )
                <h1 class="mb-6 text-primary">Ваша корзина пуста, оформление заказа невозможно.</h1>
            @else
            <h1 class="mb-6 text-primary">Оформление заказа</h1>

            <div class="flex-col">
                <form method="POST" action="{{ route('checkout.post') }}" class="flex flex-wrap">
                    @csrf
                    <div class="flex w-full lg:w-3/5">
                        <div class="w-full m-4 p-10 bg-white rounded shadow-xl">
                            <p class="text-primary font-medium mb-4">Информация о заказе</p>
                            <div class="">
                                <label class="mb-2 block text-sm text-gray-00" for="name">Имя</label>
                                <input class="w-full px-5 py-4 text-gray-700 bg-gray-200 rounded" id="name" name="name" type="text" required="" placeholder="Your Name" aria-label="Name">
                            </div>
                            <div class="mt-2 mb-2">
                                <label class="mb-2 block text-sm text-gray-600" for="phone">Телефон</label>
                                <input class="w-full px-5  py-4 text-gray-700 bg-gray-200 rounded" id="phone" name="phone" type="text" required="" placeholder="Your Phone" aria-label="Phone">
                            </div>

                            @if(env('DELIVERY'))
                            <div class="inline-block mt-2 -mx-1 pl-1 w-1/2">
                                <input type="radio" name='pickup_option' value='Доставка' data-id="delivery" />
                                <span><strong>Доставка</strong></span>
                            </div>
                            @endif
                            <div class="inline-block mt-2 w-1/2 pr-1">
                                <input type="radio" name='pickup_option' value='Самовывоз' data-id="cherrypick" @if(!env('DELIVERY'))checked @endif/>
                                <span><strong>Самовывоз</strong></span>
                            </div>

                            <div>
                                <div id="cherrypick" @if(env('DELIVERY')) style="display: none" @endif>
                                    <div class="mt-2">
                                        <label class="mb-2 text-sm text-gray-600" for="address">Выберите пункт самовывоза</label>

                                        <select class="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="stock_id" name="stock_id">

                                            @foreach($stocks as $stock)
                                                <option value="{{ $stock->id }}"> {{ $stock->address }} </option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div id="delivery" style="display: none">
                                    <div class="mt-2">
                                        <label class="mb-2 text-sm text-gray-600" for="address">Адрес</label>
                                        <input class="w-full px-2 py-2 text-gray-700 bg-gray-200 rounded" id="cus_email" name="address" type="text" placeholder="Address">
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="flex w-full lg:w-2/5">
                        <div class="w-full m-4 p-10 bg-white rounded shadow-xl">
                            <p class="text-primary font-medium mb-4">Детали заказа</p>
                            <div class="divide-y divide-grey-400">
                                <div class="flex justify-between py-4">
                                    <div>Стоимость</div>
                                    <div> {{ \Cart::getTotal() }} ₽ </div>
                                </div>
                                @if(env('DELIVERY'))
                                <div class="flex justify-between py-4">
                                    <div>Доставка</div>
                                    <div> @if(\Cart::getTotal() > env('FREE_DELIVERY') ) Бесплатно @else {{ env( 'DELIVERY_PRICE' ) }} ₽ @endif </div>
                                </div>
                                @endif
                                <div class="flex justify-between py-4">
                                    <div>Самовывоз</div>
                                    <div>  Бесплатно  </div>
                                </div>
                                <div class="flex justify-between py-4">
                                    <div>Итого</div>
                                    <div> {{ \Cart::getTotal() }} ₽ </div>
                                </div>
                            </div>

                            <div class="mt-4">
                                    <button type="submit"
                                        class="flex justify-center w-full px-10 py-3 mt-6 font-medium text-primary uppercase bg-white text-white border border-primary rounded-full shadow item-center hover:bg-primary hover:text-white focus:shadow-outline focus:outline-none">
                                        <svg aria-hidden="true" data-prefix="far" data-icon="credit-card" class="w-8"
                                             xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                            <path fill="currentColor"
                                                  d="M527.9 32H48.1C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48.1 48h479.8c26.6 0 48.1-21.5 48.1-48V80c0-26.5-21.5-48-48.1-48zM54.1 80h467.8c3.3 0 6 2.7 6 6v42H48.1V86c0-3.3 2.7-6 6-6zm467.8 352H54.1c-3.3 0-6-2.7-6-6V256h479.8v170c0 3.3-2.7 6-6 6zM192 332v40c0 6.6-5.4 12-12 12h-72c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h72c6.6 0 12 5.4 12 12zm192 0v40c0 6.6-5.4 12-12 12H236c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h136c6.6 0 12 5.4 12 12z"/>
                                        </svg>
                                        <span class="ml-2 mt-5px">Оформить заказ</span>
                                    </button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
            @endif
        </div>
    </div>

@endsection

