@extends('layouts.app', [
    'title' => 'Заказ №' . $order->id
])

@section('content')
    @include('components.navbar.navbar_white')

    <section class="section-pad-top section-pad-bottom">
{{--        <div class="container mx-auto">--}}
{{--            <h1 class="mb-6 text-primary">Спасибо, Ваш заказ № {{ $order->id }} был получен </h1>--}}
{{--            <div class="flex w-full xl:w-1/2">--}}
{{--                <table class="table-fixed w-full">--}}
{{--                    <thead>--}}
{{--                    <tr>--}}
{{--                        <th class="w-1/6 px-4 py-2 text-left">№</th>--}}
{{--                        <th class="w-3/6 px-4 py-2 text-left">Товар</th>--}}
{{--                        <th class="w-1/6 px-4 py-2 text-right">Цена</th>--}}
{{--                        <th class="w-1/6 px-4 py-2 text-right">Количество</th>--}}
{{--                    </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                    @foreach( $cart as $item )--}}
{{--                        <tr>--}}
{{--                            <td class="border px-4 py-2">{{ $item->id }}</td>--}}
{{--                            <td class="border px-4 py-2">{{ $item->name }}</td>--}}
{{--                            <td class="border px-4 py-2 text-right">{{ $item -> price }}</td>--}}
{{--                            <td class="border px-4 py-2 text-right"> {{$item->quantity}} </td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                    @if( !($order->stock_id) )--}}
{{--                        <tr>--}}
{{--                            <td class="border px-4 py-2" colspan="3">Доставка</td>--}}
{{--                            <td class="border px-4 py-2 text-right"> @if( $order->total - env('DELIVERY_PRICE') > env('FREE_DELIVERY') ) Бесплатно @else {{ env( 'DELIVERY_PRICE' ) }} ₽ @endif </td>--}}
{{--                        </tr>--}}
{{--                    @else--}}
{{--                        <tr>--}}
{{--                            <td class="border px-4 py-2" colspan="3">Самовывоз</td>--}}
{{--                            <td class="border px-4 py-2 text-right"> @if( $order->total - env('DELIVERY_PRICE') > env('FREE_DELIVERY') ) Бесплатно @else {{ env( 'DELIVERY_PRICE' ) }} ₽ @endif </td>--}}
{{--                        </tr>--}}
{{--                    @endif--}}

{{--                    <tr>--}}
{{--                        <td class="border px-4 py-2" colspan="3">Итого</td>--}}
{{--                        <td class="border px-4 py-2 text-right"> {{ $order->total }} </td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="flex flex-col w-full p-2 lg:p-8 mx-auto text-gray-800 bg-white shadow-lg pin-r pin-y lg:w-4/5">

            <h1 class="mb-6 text-primary">Ваш заказ № {{ $order->id }} принят.</h1>
            <div class="flex justify-center">
                <div class="flex w-full flex-wrap">
                    <div class="flex w-full xl:w-3/5">
                        <div class="w-full m-4 p-4 lg:p-10 bg-white rounded shadow-xl">
                            <p class="text-primary font-medium mb-4">Заказ</p>
                            <div>
                                <table class="table-auto w-full">
                                    <thead>
                                    <tr>
                                        <th class="px-4 py-2">Товар</th>
                                        <th class="px-4 py-2 text-right">Количество</th>
                                        <th class="px-4 py-2 text-right">Итого</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $cart as $item )
                                    <tr>
                                        <td class="border px-4 py-2"> {{ $item->name }} </td>
                                        <td class="border px-4 py-2 text-right"> {{ $item->quantity }} </td>
                                        <td class="border px-4 py-2 text-right"> {{ $item->price * $item->quantity }} ₽</td>
                                    </tr>
                                    @endforeach
                                    @if( ($order->address) )
                                    <tr>
                                        <td class="border px-4 py-2" colspan="2">Доставка</td>
                                        <td class="border px-4 py-2 text-right"> @if( $order->pickup_options == 'Доставка' && $order->total >= env('FREE_DELIVERY') ) Бесплатно @else {{ env( 'DELIVERY_PRICE' ) }} ₽ @endif </td>
                                    </tr>
                                    @else
                                        <tr>
                                            <td class="border px-4 py-2" colspan="2">Самовывоз</td>
                                            <td class="border px-4 py-2 text-right"> Бесплатно </td>
                                        </tr>
                                    @endif
                                    <tr>
                                        <td class="border px-4 py-2" colspan="2"> Сумма </td>
                                        <td class="border px-4 py-2 text-right"> @if($order->pickup_options == 'Доставка' && $order->total < env('FREE_DELIVERY')){{ $order->total + env('DELIVERY_PRICE') }} @else {{ $order->total }}@endif ₽</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="flex w-full xl:w-2/5">
                        <div class="w-full m-4 p-4 lg:p-10 bg-white rounded shadow-xl">
                            <p class="text-primary font-medium mb-4">Информация о получении</p>

                            @if( ($order->address) )
                                Ваш заказ будет доставлен по адресу  <span class="text-primary">{{ $order->address }}</span>

                            @else
                              Ваш заказ доступен для самовывоза по адресу <span class="text-primary">{{ $order->stock->address }}</span>
                            @endif

                            <div class="mt-4">
                                <a href="{{ route('categories.index') }}"
                                        class="flex justify-center w-full px-10 py-3 mt-6 font-medium text-primary uppercase bg-white text-white border border-primary rounded-full shadow item-center hover:bg-primary hover:text-white focus:shadow-outline focus:outline-none">
                                    <svg class="h-8 w-8" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M29.8463 3.1875H26.3559C26.3229 3.1875 26.2949 3.2029 26.2628 3.20628C26.1959 3.21312 26.1333 3.22423 26.0706 3.24512C26.0181 3.26247 25.9707 3.28388 25.9228 3.30987C25.8677 3.33984 25.8181 3.37237 25.7701 3.41286C25.7259 3.44997 25.6884 3.48948 25.6519 3.53403C25.6143 3.57993 25.5821 3.62665 25.5536 3.67984C25.5241 3.73445 25.5033 3.79049 25.4852 3.85104C25.4754 3.88386 25.4541 3.91023 25.4478 3.94471L24.9192 6.83376L2.33004 8.15385C2.2435 8.14784 2.16717 8.16046 2.07042 8.16947C1.56862 8.21514 1.19602 8.65505 1.23328 9.15745C1.23629 9.19531 1.24109 9.23197 1.2483 9.26803L2.82222 17.8522C3.1822 19.1833 4.29097 20.527 5.95864 20.527H22.4135L22.0612 22.4525H8.21525C6.46225 22.4525 5.03556 23.8792 5.03556 25.6322C5.03556 27.3858 6.46225 28.8125 8.21525 28.8125C9.96826 28.8125 11.3949 27.3858 11.3949 25.6322C11.3949 25.1545 11.2814 24.7056 11.0918 24.2987H19.5941C19.4045 24.7056 19.291 25.1545 19.291 25.6322C19.291 27.3858 20.7177 28.8125 22.4707 28.8125C24.2237 28.8125 25.6503 27.3858 25.6503 25.6322C25.6503 24.3857 24.9224 23.3153 23.8754 22.7943L27.1253 5.03365H29.8463C30.3559 5.03365 30.7693 4.62019 30.7693 4.11058C30.7693 3.60096 30.3559 3.1875 29.8463 3.1875ZM23.4119 15.0705L19.5003 15.132L19.7133 12.865L23.8382 12.7414L23.4119 15.0705ZM3.87 13.34L7.77888 13.2228L8.03459 15.312L4.24192 15.3715L3.87 13.34ZM9.0137 13.1858L13.3102 13.057V15.2291L9.27167 15.2926L9.0137 13.1858ZM13.3102 11.8251L8.86346 11.9585L8.57936 9.638L13.3102 9.3614V11.8251ZM14.541 9.28944L18.8366 9.03824L18.5897 11.6668L14.541 11.7882V9.28944ZM13.3102 16.4602V18.6809H9.68648L9.42206 16.5213L13.3102 16.4602ZM14.541 16.4409L18.1464 16.3843L17.9305 18.6809H14.541V16.4409ZM14.541 15.2098V13.0201L18.4736 12.9022L18.2622 15.1514L14.541 15.2098ZM24.0648 11.5027L19.8294 11.6297L20.0797 8.9656L24.5772 8.7026L24.0648 11.5027ZM7.34874 9.70996L7.62856 11.9954L3.64562 12.1149C3.4778 11.203 3.34416 10.4944 3.23028 9.95072L7.34874 9.70996ZM4.6215 17.4453L4.4666 16.5991L8.18505 16.5407L8.44707 18.6809H5.95864C5.16778 18.6809 4.72727 17.8239 4.6215 17.4453ZM19.1668 18.6809L19.3844 16.3649L23.186 16.3051L22.7513 18.6809H19.1668ZM9.54879 25.6322C9.54879 26.3678 8.95083 26.9663 8.21525 26.9663C7.47967 26.9663 6.88172 26.3678 6.88172 25.6322C6.88172 24.8966 7.47967 24.2987 8.21525 24.2987C8.95083 24.2987 9.54879 24.8966 9.54879 25.6322ZM22.4707 26.9663C21.7351 26.9663 21.1371 26.3678 21.1371 25.6322C21.1371 24.8966 21.7351 24.2987 22.4707 24.2987C23.2062 24.2987 23.8042 24.8966 23.8042 25.6322C23.8042 26.3678 23.2062 26.9663 22.4707 26.9663Z" fill="currentColor"/>
                                    </svg>
                                    <span class="ml-2 mt-1">Продолжить покупки</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </section>
@endsection
