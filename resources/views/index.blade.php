@extends('layouts.app')

@section('content')
    @include('components.navbar.navbar_primary')
    @include('components.intro')
    @include('components.locator')
    @include('components.sales')
    @include('components.form')
    @include('components.advantages')
@endsection
