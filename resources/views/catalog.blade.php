@extends('layouts.app', [
    'title' => 'Каталог'
])
@section('content')
    @include('components.navbar.navbar_white')

    <section class="container mx-auto section-pad-bottom section-pad-top">
        <h1 class="mb-4">
            Каталог товаров
        </h1>
        <h2>
            Последнее обновление - <span class="text-primary underline">{{  date('d-m-Y')  }}</span>
        </h2>

        <div class="main w-full lg:w-4/5 mx-auto mt-6 mb-8">
            <div class="search">
                <div class="flex items-center border text-lg border-primary pr-4 w-full rounded-full">
                    <input type="text" name="catalogSearch" id="catalogSearch" placeholder="Поиск..."
                           class="w-full py-2 pl-4 rounded-full"/>
                    <span class="flex-shrink-0  text-sm py-1 px-2 rounded" id="clearSearch">
                               <img class=" h-6 mx-auto my-auto" src="{{ asset('/images/close.svg') }}">
                    </span>
                </div>

                <div class="overflow-y-auto results">
                    <div class=" text-decoration-none py-4" id="catalogSearchRes"></div>
                </div>
            </div>
        </div>


        <div class="mx-auto">
            <div class="md:flex mb-4 md:flex-wrap">
                @foreach($categories as $category)
                    <div class="w-full md:w-1/2 lg:w-1/4 mx-auto h-48">
                        <div
                            class="rounded-lg border-2 border-solid border-primary mx-3 px-6 pt-3 pb-3">
                            <a href="{{ route('categories.show', $category) }}"
                               class="block text-xl text-center font-bold"
                            >
                                <div class="flex-1">
                                    {{ $category->name }}
                                </div>
                            </a>
                            <div class="flex mt-8 mb-6">
                                <div class="line w-full border border-primary h-px border-white my-auto mr-4"></div>
                                <img class="h-10" src="{{ asset('/images/catalog.svg') }}">
                                <div class="line w-full border border-primary h-px border-white my-auto ml-4"></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <script type="application/javascript">

        $(document).ready(function () {


            $('#catalogSearch').on('keyup', function () {

                let search = $('#catalogSearch').val();
                // console.log(search.length)
                if (search.length > 3) {
                    $.ajax({

                        type: "GET",
                        url: '/search',
                        data: {search: $('#catalogSearch').val()},
                        success: function (data) {
                            $("#catalogSearchRes").empty();
                            $('.results').show()
                            $.each(data, function (i, item) {
                                let productUrl = '{{ route('products.index') }}/' + item.id;
                                $("#catalogSearchRes").append('<a class="w-full" href=" ' + productUrl + ' " >' +

                                    '<div class="flex w-full px-4 mt-2">\n' +
                                    '    <div class="flex-col">\n' +
                                    '        <div class="flex">' + item.name + '</div>\n' +
                                    '        <small class="flex text-primary">В наличии ' + item.qty + ' шт. </small>\n' +
                                    '    </div>\n' +
                                    '    <div class="flex ml-auto">от ' + item.price + '₽</div>\n' +
                                    '</div>' +
                                    '</a> <br/>');
                            });


                        }
                    });
                } else {
                    $("#catalogSearchRes").empty();
                    $('.results').hide()
                }
            });

            $("#clearSearch").click(function () {
                $("#catalogSearch").val('');
                $('.results').hide()
                $("#catalogSearchRes").empty();
            });


        });
    </script>


@endsection

