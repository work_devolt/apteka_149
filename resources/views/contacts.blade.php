@extends('layouts.app', [
    'title' => 'Адреса аптек'
])
@section('content')
    @include('components.navbar.navbar_white')

    <section class="container mx-auto section-pad-bottom section-pad-top">
        <h1  class="mb-4">
            Адреса наших аптек
        </h1>
        <h2>
            Последнее обновление - <span class="text-primary underline">{{  date('d-m-Y')  }}</span>
        </h2>



            <div class="mx-auto mt-6">
            <div class="flex flex-wrap mb-6 w-full">                    
                @foreach($stocks as $stock)
                        
                        <div class="bg-white px-3 lg:p-4 mb-6 w-full md:w-1/2 lg:w-1/3 xl:w-1/3">
                            <div class="w-full shadow rounded-xl ">
                                <div class="w-full">
                                    <img class="h-auto lg:h-56 w-full object-cover object-center rounded-t-xl"
                                    src="{{ ($stock->image) ? Voyager::image( $stock->image ) : asset('/images/placeholder.svg')}}"
                                    alt="">
                                </div>
    
                                <div class="pt-6 pb-12 px-6">
                                    <div class="text-xl lg:text-2xl font-bold mb-2 text-primary">
                                        {{ $stock->address }}
                                    </div>
                                    <div class="text-sm leading-relaxed block text-lg text-grey-600">
                                    <div>Телефон: <a class="underline text-primary" href="tel:">{{$stock->phone}}</a></div>
                                    <div>Режим работы: {{ $stock->work_hours }}</div>
                                    <div class="mt-2"><a href="{{ $stock->map_link }}" class="text-primary text-xl underline">На карте</a></div>
    
                                    </div>
                                </div>
                            </div>
                        </div>  
                    @endforeach
                </div>
            </div>
    </section>



@endsection

