@extends('layouts.app', [
    'title' => $category->name
])

@section('content')
    @include('components.navbar.navbar_white')

    <section class="container mx-auto section-pad-bottom section-pad-top">
        <h1  class="mb-4">
            <a class="underline" href="{{route('categories.index')}}">Каталог</a> / <span class="text-primary">{{ $category->name }}</span>
        </h1>
        <div class="mt-6">

            <div class="container mx-auto">
                <div class="md:flex mb-8 md:flex-wrap justify-center">
                    @foreach($products as $product)
                        @include('components.products._card')
                    @endforeach
                </div>
                {{ $products->links() }}
            </div>

        </div>
    </section>

@endsection
