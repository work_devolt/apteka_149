@extends('layouts.app')

@section('content')
    @include('components.navbar.navbar_white')

    <section class="container mx-auto section-pad-bottom section-pad-top">
        <h1  class="mb-6">
            Админ-панель заказов
        </h1>

        <div class="mt-6">

            <div class="container mx-auto">

                <!-- component -->
                <div class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 pr-10 lg:px-8">
                    <div class="align-middle rounded-tl-lg rounded-tr-lg inline-block w-full py-4 overflow-hidden bg-white shadow-lg px-12">
                        <div class="flex justify-between">

                                <form action="{{ route('home.search') }}">
                                    <div class="flex w-full">
                                        <input type="text" name="search" class="w-4/5 border border-primary rounded px-3 relative " placeholder="Search">
                                        <button type="submit" class="ml-2 border border-primary py-2 px-4 w-32 rounded">Поиск</button>
                                    </div>

                                </form>
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button class="text-primary" type="submit">Выход</button>
                            </form>
                        </div>

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    </div>
                    <div class="align-middle inline-block min-w-full shadow overflow-hidden bg-white shadow-dashboard px-8 pt-3 rounded-bl-lg rounded-br-lg">
                        <table class="w-full table-auto my-8">
                            <thead>
                            <tr class="text-primary">
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left leading-4 text-primary tracking-wider">ID</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 tracking-wider">Имя</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 tracking-wider">Телефон</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 tracking-wider">Адрес</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 tracking-wider">Товары</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-left text-sm leading-4 tracking-wider">Сумма</th>
                                <th class="px-2 py-3 border-b-2 border-gray-300 text-right text-sm leading-4 tracking-wider">Статус</th>
                            </tr>
                            </thead>
                            <tbody class="bg-white">
                            @foreach($orders as $order)
                            <tr class="border-b border-grey-500">
                                <td class="px-2 py-4">
                                    <div class="flex items-center">
                                        <div>
                                            <div class="text-sm leading-5 text-gray-800">#{{ $order->id }}</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="px-2 py-4">
                                    <div class="text-sm leading-5 text-blue-900">{{ $order->name }}</div>
                                </td>
                                <td class="px-2 py-4">
                                    {{ $order->phone }}</td>
                                <td class="px-2 py-4 text-sm leading-5">
                                    @if( ($order->address) )
                                        Доставка:  <span class="text-primary">{{ $order->address }}</span>

                                    @else
                                        Cамовывоз <span class="text-primary"> {{ $order->stock->name }} </span>
                                    @endif
                                </td>
                                <td class="px-2 py-4 text-sm leading-5"><div>
                                        @foreach( json_decode($order->cart) as $item )
                                            <div class="flex-column">
                                                <div>{{$item->name}}</div>
                                                <span class="text-primary"><small>Кол-во: {{ $item->quantity }} </small></span>
                                            </div>

                                        @endforeach

                                    </div></td>
                                <td class="px-2 py-4  text-sm leading-5"> {{ $order->total }} </td>
                                <td class="mx-auto py-4 text-sm leading-5">
                                    <div>
                                        <form class="flex-col text-center" method="POST" action="{{ route('updateStatus') }}">
                                            @csrf

                                            <input type="text" hidden name="id" value="{{ $order->id }}">
                                            <select name="status" class="block appearance-none w-full bg-white border border-primary text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" onchange="this.form.submit()">
                                                <option value disabled selected>
{{--                                                    @if($order->status == 'NEW') Новый--}}
{{--                                                    @elseif($order->status == 'IN_WORK') В работе--}}
{{--                                                    @elseif($order->status == 'FINISHED') Выполнен--}}
{{--                                                    @endif--}}
                                                    {{ $order->status }}
                                                </option>
                                                <option value="Новый">Новый</option>
                                                <option value="В работе">В работе</option>
                                                <option value="Выполнен">Выполнен</option>
                                            </select>
                                        </form>

                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}

                    </div>
                </div>

            </div>

        </div>
    </section>



@endsection


