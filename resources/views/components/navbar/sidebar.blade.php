<div id="sidebar" class="w-full lg:w-2/5 xl:w-1/5 text-primary bg-white p-4 relative h-screen shadow-lg">
    <div class="absolute top-0 right-0 p-4 sidebar-btn">
        <img class=" h-6 mx-auto my-auto"  src="{{ asset('/images/close.svg') }}">
    </div>

    <div class="flex-row pt-16 pl-4">
        <nav>
            <ul>
                <li class="pb-4">
                    <a href="/" class="text-2xl font-semibold"> Главная </a>
                </li>
                <li class="pb-4">
                    <a href="{{ route('categories.index') }}" class="text-2xl font-semibold"> Каталог </a>
                </li>
                <li class="pb-4">
                    <a href="{{ route('contacts') }}" class="text-2xl font-semibold"> Адреса аптек </a>
                </li>
                @foreach($pages as $item)
                    <li class="pb-4">
                        <a href="{{ $item->slug }}" class="text-2xl font-semibold"> {{ $item->name }} </a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
    <div class="absolute bottom-0 left-0 p-6">
        <a class="text-2xl" href="tel:+7-988-291-11-49">+7-988-291-11-49</a>
    </div>
</div>
