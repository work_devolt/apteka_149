<div id="searchbar" class="w-full lg:w-2/5 xl:w-1/5 text-primary bg-white p-4 relative h-screen shadow-lg">
    <div class="absolute top-0 right-0 p-4 searchbar-btn">
        <img class=" h-6 mx-auto my-auto"  src="{{ asset('/images/close.svg') }}">
    </div>

    <div class="flex-row pt-16 pl-4">
        <form class="w-full max-w-sm">
            <div class="flex items-center border-b border-primary py-2">
                <input id="txtSearch" class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none" type="text" placeholder="Поиск" >
            </div>
        </form>

        <div class="overflow-y-auto mt-2" style="height: 100vh;">
            <div class="p-2" id="search-result"></div>
        </div>

    </div>

</div>
