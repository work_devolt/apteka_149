<div class="relative">
    <section class="intro relative bg-primary text-white pt-24 lg:pt-40 pb-64" >
        <div class="container mx-auto">
            <h1 class="text-4xl md:text-6xl text-center md:text-left leading-tight pb-5 lg:pb-6">
                Качество, <br>
                доступное каждому!
            </h1>

            <div class="text-center md:text-left">
                <a href="{{ route('categories.index') }}">
                    <button class="bg-primary border border-white text-white text-2xl md:text-4xl xl:text-2xl py-4 px-8 mb-8 lg:mb-0 rounded-full inline-flex items-center">
                        <span>Каталог товаров</span>
                        <svg class="fill-current w-8 h-8 ml-10" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M23.5757 15.3473L9.72953 1.50115C9.36895 1.14057 8.78482 1.14057 8.42424 1.50115C8.06366 1.86172 8.06366 2.44586 8.42424 2.80643L21.6178 15.9999L8.42424 29.1935C8.06366 29.554 8.06366 30.1382 8.42424 30.4987C8.60453 30.679 8.84011 30.7692 9.07689 30.7692C9.31366 30.7692 9.54924 30.679 9.72953 30.4987L23.5757 16.6526C23.9363 16.292 23.9363 15.7079 23.5757 15.3473Z" fill="currentColor"/>

                        </svg>
                    </button>
                </a>
            </div>


        </div>

        <img src="{{ asset('/images/swiss.png') }}" class="swiss h-64 xl:h-auto">
    </section>
    <div class="triangle-bottom bg-primary w-full h-12 lg:h-32 " style="clip-path: polygon(100% 0, 0 0, 100% 100%);  -webkit-clip-path: polygon(100% 0, 0 0, 100% 100%); margin-top:-1px;"></div>
    <a href="#locator">
        <div class="w-10 h-10 absolute rounded-full border border-primary bg-white text-primary text-center -mt-6 lg:-mt-16 " style="transform: translate(-50%, -50%); top: 100%; left: 50%;">
            <svg class="transform rotate-90 h-6 w-6 mx-auto mt-2" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M23.5757 15.3473L9.72953 1.50115C9.36895 1.14057 8.78482 1.14057 8.42424 1.50115C8.06366 1.86172 8.06366 2.44586 8.42424 2.80643L21.6178 15.9999L8.42424 29.1935C8.06366 29.554 8.06366 30.1382 8.42424 30.4987C8.60453 30.679 8.84011 30.7692 9.07689 30.7692C9.31366 30.7692 9.54924 30.679 9.72953 30.4987L23.5757 16.6526C23.9363 16.292 23.9363 15.7079 23.5757 15.3473Z" fill="currentColor"/>
            </svg>
        </div>
    </a>


</div>

