<footer id="footer" class="footer pt-24 text-white ">
    <div class="pt-4 md:pt-8 md:pb-8 pb-6 bg-primary relative">

        <img src="{{ asset('/images/footer-bg.svg') }}" class="footer-bg h-auto">

        <div class="flex container mx-auto pb-1 pt-6 sm:flex justify-between">
            <div class="mx-auto lg:mx-0">
                <a href="/">
                    <img src="{{ asset('/images/logo.svg') }}" class="h-12 lg:h-16" />
                </a>
            </div>

            <div class="flex text-right hidden lg:flex my-auto">
                <a href="tel:{{ env('PHONE') }}">
                    <button class="ml-8 bg-white border-2 border-primary text-primary text-2xl py-4 px-8 rounded-full inline-flex items-center">
                        Свяжитесь с нами
                        <svg class="fill-current w-8 h-8 ml-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M23.5757 15.3473L9.72953 1.50115C9.36895 1.14057 8.78482 1.14057 8.42424 1.50115C8.06366 1.86172 8.06366 2.44586 8.42424 2.80643L21.6178 15.9999L8.42424 29.1935C8.06366 29.554 8.06366 30.1382 8.42424 30.4987C8.60453 30.679 8.84011 30.7692 9.07689 30.7692C9.31366 30.7692 9.54924 30.679 9.72953 30.4987L23.5757 16.6526C23.9363 16.292 23.9363 15.7079 23.5757 15.3473Z" fill="currentColor"/>

                        </svg>
                    </button>
                </a>
            </div>
        </div>
        <div class="flex flex-wrap container mx-auto pt-6 justify-content-between">
            <div class="w-full w-1/2 md:w-1/4 text-center md:text-left">
                <div class="uppercase text-xl font-bold">
                    Аптека 149
                </div>
                <div class="mb-4 text-lg">
                    <nav>
                        <ul>
                            <li>
                                <a href="#">Главная</a>
                            </li>
                            <li>
                            <a href="{{ route('contacts') }}">Адреса аптек</a>
                            </li>
                            @foreach($pages as $item)
                                <li class="pb-4">
                                    <a href="{{ $item->slug }}"> {{ $item->name }} </a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="w-full md:w-1/4 ml-0 md:ml-8 text-center md:text-left">
                <div class="text-xl font-bold">
                    Каталог товаров
                </div>
                <div class="mb-4 text-lg">
                    <nav>
                        <ul>
                            @foreach($rootCategories as $category)
                            <li>
                                <a href="{{ route('categories.show', $category) }}">
                                    {{ $category->name }}
                                </a>
                            </li>
                            @endforeach
                            <li class="underline">
                                <a href="{{ route('categories.index') }}">
                                    Все категории
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="w-full md:w-1/4 text-center ml-auto">
                <div class="text-xl font-bold">
                    Наши соцсети
                </div>
                <div class="flex mt-4 justify-center">
                    <a target="_blank" a href="https://instagram.com/apteka149?igshid=1eiv730smndo"><img src="{{ asset('/images/socials/instagram.svg') }}" class="transform hover:scale-110 transition duration-300 mx-2 h-10 w-10">
                    </a>
                    <a target="_blank" href="https://t.me/apteka149"><img src="{{ asset('/images/socials/telegram.svg') }}" class="transform hover:scale-110 transition duration-300 mx-2 h-10 w-10">
                    </a>
                    <a target="_blank" href="https://wa.me/79882911149"><img src="{{ asset('/images/socials/whatsapp.svg') }}" class="transform hover:scale-110 transition duration-300 mx-2 h-10 w-10">
                    </a>

                </div>
            </div>

        </div>
    </div>
    <div class="bg-primary-darken">
        <div class="container mx-auto py-4">
            <div class="flex">
                <div class="w-1/2 text-left text-xs underline">
                    Политика конфиденциальности
                </div>
                <div class="w-1/2 text-right text-xs">

                        Дизайн и разработка
                    <a class="underline" target="_blank" href="#">
                        Den Tikhonov
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
