<template>
    <section class=" section-pad-bottom section-pad-top container mx-auto">
            <h1  class="mb-4">
                Наши преимущества
            </h1>
            <h2>
                Почему именно мы <span class="text-primary">?</span>
            </h2>
        <div class="flex flex-wrap sm:max-w-md md:max-w-6xl mx-auto">
            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "car", 'text' => "Бесплатная доставка"])
            </div>

            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "case", 'text' => "Официальные поставщики"])
            </div>

            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "drugs", 'text' => "Большой ассортимент"])
            </div>

            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "employee", 'text' => "Качественная консультация"])
            </div>

            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "hands", 'text' => "Забота о клиенте"])
            </div>

            <div class="w-full xs:mx-auto md:w-1/2 lg:w-1/3">
                @include('components.advantages._item', ['icon' => "sale", 'text' => "Низкие цены"])
            </div>
        </div>




    </section>
</template>
