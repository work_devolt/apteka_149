
    <section id="locator" class="container mx-auto section-pad-bottom section-pad-top">
        <h1  class="mb-4">
            Аптеки рядом с Вами
        </h1>
        <h2>
            Последнее обновление - {{  date('d-m-Y')  }} - <span class="text-primary underline">Обновить</span>
        </h2>

        <div class="mt-6">
            @include('components.stores._card')
        </div>
    </section>

