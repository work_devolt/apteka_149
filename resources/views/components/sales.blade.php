<template>
    <section class="sales section-pad-bottom section-pad-top">
        <div class="container mx-auto">
            <h1  class="mb-4">
                Акции
            </h1>
            <h2>
                Лучшие предложения недели
            </h2>
        </div>


        <div class="mt-6 flex overflow-x-scroll scrolling-touch">
            @foreach($products as $product)
                @include('components.products._card')
            @endforeach
        </div>


    </section>
</template>
