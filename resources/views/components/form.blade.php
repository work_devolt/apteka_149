<div class="triangle-bottom bg-primary w-full h-12 lg:h-32" style="clip-path: polygon(100% 100%, 0 0, 0 100%); margin-bottom:-1px;"></div>
<section
    class="section section-pad-bottom section-pad-top bg-primary text-white"
>


    <div class="container mx-auto">
        <h1 class="mb-4">
            Свяжитесь с нами
        </h1>
        <h2>
            Есть <span class="underline">вопросы</span> или хотите <span class="underline">заказать препарат</span> ?
        </h2>


        <div class="rounded-lg border-2 border-solid px-8 pt-6 pb-4 mt-16 mb-8">
            <div class="flex flex-col lg:flex-row my-4 form">


                <input class="mt-4 py-4 px-4 shadow text-lg font-bold border-2 rounded-full w-full py-2 px-3 text-white bg-primary" id="username" name="name" type="text" placeholder="Имя*">

                <input class="mt-4 xl:ml-8 shadow text-lg font-bold border-2 rounded-full w-full py-4 px-3 text-white bg-primary" id="phone" name="phone" type="text" placeholder="Телефон*">

                <button class="mt-4 xl:ml-8 w-full bg-white border-2 border-primary text-primary text:lg lg:text-2xl py-4 px-8 rounded-full inline-flex items-center form-submit">
                    <span>Заказать звонок</span>
                    <svg class="fill-current w-8 h-8 ml-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M23.5757 15.3473L9.72953 1.50115C9.36895 1.14057 8.78482 1.14057 8.42424 1.50115C8.06366 1.86172 8.06366 2.44586 8.42424 2.80643L21.6178 15.9999L8.42424 29.1935C8.06366 29.554 8.06366 30.1382 8.42424 30.4987C8.60453 30.679 8.84011 30.7692 9.07689 30.7692C9.31366 30.7692 9.54924 30.679 9.72953 30.4987L23.5757 16.6526C23.9363 16.292 23.9363 15.7079 23.5757 15.3473Z" fill="currentColor"/>

                    </svg>
                </button>
            </div>
            <div class="flex flex-col lg:flex-row my-8 text-center form-thanks hidden">
                <div class="text-2xl md:text-4xl px-0 md:px-8">Спасибо, ваша заявка принята, мы свяжемся с вами в ближайшее время!</div>
            </div>
            <div class="flex my-8">
                <div class="line w-full border border-solid h-px border-white my-auto mr-4"></div>
                <div class="text-4xl text-center uppercase">или</div>
                <div class="line w-full border border-solid h-px border-white my-auto ml-4"></div>
            </div>

            <div class="flex flex-col md:flex-row md:justify-center">
                <div class="flex flex-col justify-center text-center mb-4 md:mb-0">
                    <span class="md:pr-4 sm:text-2xl font-light">
                        Позвоните нам:
                    </span>
                </div>
                <a href="tel:+79882911149">
                    <button class="lg:ml-8 w-full bg-white border-2 border-primary text-primary text:lg lg:text-2xl py-4 px-6 rounded-full inline-flex items-center">
                        +7-9882-<span class="underline mr-4">911-149</span>
                        <svg class="fill-current ml-auto w-8 h-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M30.1503 23.6754C29.6539 22.8653 28.8835 21.9218 27.9784 21.018C27.3234 20.3629 25.0746 18.2271 23.6286 18.2271C23.1647 18.2271 22.8702 18.4254 22.7056 18.5901L20.7801 20.5144C20.7476 20.5468 20.6443 20.5841 20.458 20.5841C19.3895 20.5841 17.1238 19.4014 14.8606 17.1394C11.8426 14.1201 11.1166 11.5889 11.4856 11.2199L13.4087 9.29562C13.5746 9.12975 13.7729 8.83528 13.7729 8.37254C13.7741 6.92542 11.6371 4.67663 10.982 4.02158C10.3282 3.36653 8.08057 1.23071 6.63225 1.23071C6.16951 1.23071 5.87504 1.42783 5.70918 1.59369L1.85942 5.44345C0.722399 6.57927 1.13706 8.80163 3.09259 12.048C4.78971 14.8653 7.476 18.1682 10.6539 21.3461C16.2909 26.9831 21.9988 30.7704 24.857 30.7692C25.5686 30.7692 26.1395 30.5576 26.5565 30.1406L30.4063 26.2908C31.2392 25.4579 30.4195 24.1165 30.1503 23.6754ZM25.2512 28.8353C25.208 28.8773 25.083 28.923 24.857 28.923C22.9111 28.923 17.7296 25.81 11.9592 20.0408C8.88346 16.9651 6.29692 13.7884 4.67432 11.0949C2.8462 8.06124 3.01807 6.89538 3.16471 6.74874L6.7885 3.12494C7.26086 3.30764 8.35581 4.00475 9.67673 5.32687C10.9976 6.64898 11.696 7.74273 11.8786 8.21509L10.1803 9.91461C8.4099 11.685 10.6887 15.5769 13.5553 18.4447C16.0145 20.9038 18.6599 22.4302 20.458 22.4302C21.2981 22.4302 21.8065 22.0985 22.0854 21.8197L23.7849 20.1213C24.2573 20.304 25.351 21.0023 26.6731 22.3233C27.994 23.6442 28.6924 24.7391 28.875 25.2115L25.2512 28.8353Z" fill="currentColor"/>
                        </svg>
                    </button>
                </a>
            </div>

        </div>
        <div class="text-center text-xs mt-4 px-4">
            <p>
                Нажимая кнопку «Заказать звонок», вы соглашаетесь с условием оферты и даете согласие на обработку
                персональных данных
            </p>
        </div>

    </div>
</section>
<div class="triangle-bottom bg-primary w-full h-12 lg:h-32" style="clip-path: polygon(100% 0, 0 0, 100% 100%); margin-top:-1px;"></div>

