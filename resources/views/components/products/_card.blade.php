<!-- component -->
<a href="{{ route('products.show', $product) }}" class="transform hover:scale-105 transition duration-500">
    <div class="mx-2 flex flex-wrap items-center justify-center">
        <div class="flex-shrink-0 w-64 mx-2 my-6 relative overflow-hidden bg-primary rounded-lg max-w-xs shadow-lg">
            <svg class="absolute bottom-0 left-0 mb-8" viewBox="0 0 375 283" fill="none" style="transform: scale(1.5); opacity: 0.1;">
                <rect x="159.52" y="175" width="152" height="152" rx="8" transform="rotate(-45 159.52 175)" fill="white"/>
                <rect y="107.48" width="152" height="152" rx="8" transform="rotate(-45 0 107.48)" fill="white"/>
            </svg>
            <div class="relative pt-10 px-10 flex items-center justify-center">
                <div class="block absolute w-48 h-48 bottom-0 left-0 -mb-24 ml-3" style="background: radial-gradient(black, transparent 60%); transform: rotate3d(0, 0, 1, 20deg) scale3d(1, 0.6, 1); opacity: 0.2;"></div>
                <img class="relative w-40" src="{{ asset('/images/placeholder.png') }}" alt="">
            </div>
            <div class="relative text-white px-4 pb-4 mt-6">
                <span class="block opacity-75 mb-1">{{ $product->mainCategory->name }}</span>
                <div class="flex justify-between">
                    <span class="block font-semibold text-lg pr-2">{{ $product->name }}</span>

                    <span class="whitespace-no-wrap mt-auto h-8 block bg-white rounded-full text-primary text-xs font-bold px-3 py-2 leading-none flex items-center">от {{ $product->price }} ₽</span>
                </div>
            </div>
        </div>
    </div>
</a>

