<div class="flex flex-row px-8 py-3 md:pl-12 lg:pl-24 md:py-6 text-lg md:text-xl">
    <img src="{{ asset('/images/advantages/'.$icon.'.svg') }}" class="h-12 w-16 lg:h-16 lg:w-20" />
    <span class="ml-4 self-center">{{ $text }}</span>
</div>
