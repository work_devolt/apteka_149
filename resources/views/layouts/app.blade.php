<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{('/images/favicon.ico') }}" type="image/x-icon">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ @$title ? $title . ' - ' . setting('site.title', config('app.name')) : setting('site.title', config('app.name')) }}</title>
    <meta name="description" content="{{ @$description ? $description : setting('site.title') }}">
    <meta name="Keywords" content="{{ @$keywords ? $keywords : setting('site.title') }}">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T5JQ5TT');
    </script>
    <!-- End Google Tag Manager -->


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sidebar/3.3.2/jquery.sidebar.min.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">



</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T5JQ5TT"
                      height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="app">

        <div>
            @include('components.navbar.searchbar')
            @include('components.navbar.sidebar')

        </div>


        <main>
            @yield('content')
        </main>


        @include('components.footer')
    </div>
</body>

<script>
    $(document).ready(function() {
        $('.sidebar-btn').on('click', function() {

            $('#sidebar').toggle(300, function () {
                $('#sidebar').siblings().hide();
            });

        });

        $('.searchbar-btn').on('click', function() {
            $('#searchbar').toggle(300, function () {
                $('#searchbar').siblings().hide();
            });

        });


        $(':radio').change(function (event) {
            let id = $(this).data('id');
            $('#' + id).show().siblings().hide();

        });



    });

</script>

<script>
    $(document).ready(function() {

        $('.form-submit').on('click', function (event) {
            console.log('Opa')
            event.preventDefault();
            let name = $("input[name=name]").val();
            let phone = $("input[name=phone]").val();
            var url = '{{ route('sendForm')}}'
            $.ajax({
                method: 'GET',
                url: url,
                data: {name: name, phone: phone},
            })
                .done(function (success) {
                    $('.form').hide();
                    $('.form-thanks').show();
                })
                .fail(function (msg) {
                    console.log('Failed');
                });
        });
    });
</script>

<script type="application/javascript">
    $(document).ready(function(){

        $('#txtSearch').on('keyup', function(){

            var search = $('#txtSearch').val();
            console.log(search.length)
            if (search.length > 3) {
                $.ajax({

                    type:"GET",
                    url: '/search',
                    data: {search: $('#txtSearch').val()},
                    success: function(data) {
                        $("#search-result").empty();
                        $.each(data,function(i, item){
                            let productUrl = '{{ route('products.index') }}/'+ item.id;
                            $("#search-result").append('<a class="w-full py-2" href=" ' + productUrl + ' " >' +

                                '<div class="flex w-full">\n' +
                                '    <div class="flex-col">\n' +
                                '        <div class="flex">' + item.name + '</div>\n' +
                                '        <small class="flex text-black">В наличии ' + item.qty + ' шт. </small>\n' +
                                '    </div>\n' +
                                '    <div class="flex ml-auto"> от ' + item.price + '₽</div>\n' +
                                '</div>' +
                                '</a> <br/>');
                        });


                    }
                });
            }



        });

    });
</script>

</html>
