<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('index');
Route::get('/contacts', 'StockController@index')->name('contacts');

Route::get('/home', 'HomeController@home')->name('home')->middleware('auth');


Route::resource('products', 'ProductController');
//Route::resource('stocks', 'StockController');
Route::resource('categories', 'CategoryController');

Route::get('/cart', 'CartController@index')->name('cart.index');
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');
Route::get('/cart/add/{id}', 'CartController@add')->name('cart.add');
Route::post('/cart/update', 'CartController@update')->name('cart.update');


Route::get('/cart', 'CartController@index')->name('cart.index');
Route::get('/cart/delete/{id}', 'CartController@delete')->name('cart.delete');
Route::get('/cart/add/{id}', 'CartController@add')->name('cart.add');
Route::post('/cart/update', 'CartController@update')->name('cart.update');

Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
Route::post('/checkout', 'CheckoutController@post')->name('checkout.post');
Route::get('/checkout/{order}', 'CheckoutController@view')->name('checkout.view');

Route::get('search', 'ProductController@search');


Route::get('/send-form', 'TelegramBotController@sendForm')->name('sendForm');


//Route::get('/updated-activity', 'TelegramBotController@updatedActivity');
Route::get('products-update', 'ProductController@productsUpdate');
Route::get('stock-update', 'StockController@stockUpdate');

Auth::routes(['register' => false]);


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::middleware(['auth'])->group(function () {
    Route::get('/home/search', 'HomeController@search')->name('home.search')->middleware('auth');
    Route::post('/home/update', 'HomeController@updateStatus')->name('updateStatus')->middleware('auth');

});

Route::get('/{slug}', 'PageController@show');
