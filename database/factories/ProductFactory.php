<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'sku' => $faker->unique()->randomNumber(6),
        'description' => $faker->text(30),
        'manufacturer' => $faker->company,
        'price' => $faker->randomNumber(3),
        'qty' => $faker->randomNumber(3),
    ];
});
