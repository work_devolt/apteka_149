<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersSetStockIdIsNullOnDelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['stock_id']);
            $table->foreign('stock_id')->references('id')->on('stocks')->nullOnDelete();
        });
        Schema::table('availabilities', function (Blueprint $table) {
            $table->dropForeign(['stock_id']);
            $table->foreign('stock_id')->references('id')->on('stocks')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['stock_id']);
            $table->foreign('stock_id')->references('id')->on('stocks');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['availabilities']);
            $table->foreign('stock_id')->references('id')->on('stocks');
        });
    }
}
