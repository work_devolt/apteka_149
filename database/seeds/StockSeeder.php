<?php

use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stocks')->insert(array(
            [
                'name' => 'parafarm',
                'address' => 'г. Дербент, пр. Агасиева 17А',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'xizroeva',
                'address' => 'г. Дербент, ул. Хизроева 8',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'agasieva26',
                'address' => 'г. Дербент, пр. Агасиева 26',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'agasieva17',
                'address' => 'г. Дербент, пр. Агасиева 17 А',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'lenina86',
                'address' => 'г. Дербент, ул. Ленина 86',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => '345',
                'address' => 'г. Дербент, ул. 345 ДСД 17',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'salmana',
                'address' => 'г. Дербент, ул. Сальмана 87А',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'ogni',
                'address' => 'г. Даг. Огни, ул. Аллея Дружбы 1В',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'globus',
                'address' => 'г. Махачкала, ТЦ Глобус',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],
            [
                'name' => 'pushkina',
                'address' => 'г. Дербент, ул. Пушкина 50',
                'work_hours' => '9.00 - 22.00',
                'phone' => env('PHONE'),
                'map_link' => 'test'
            ],

        ));
    }
}
