<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Product;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 15)->create()->each(function ($category) {
            $category->products()->saveMany(factory(Product::class, 100)->make());
        });
    }
}
