<?php

namespace App\Observers;

use App\Product;
use Illuminate\Support\Facades\DB;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function creating(Product $product)
    {
        $excluded = Product::getExcludedArray();
        if (in_array($product->sku, $excluded)) {
            $product->hidden = true;
        }
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function updating(Product $product)
    {
        if ($product->hidden == true) {

            $value = setting('site.ex_products'). ','. $product->sku;
            
            DB::table('settings')->where('key', '=', "site.ex_products")->update(array('value' => $value));

        }
    }
    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
