<?php

namespace App\Console\Commands;

use App\Category;
use App\Product;
use App\Stock;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Telegram\Bot\Laravel\Facades\Telegram;


class UpdateCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'catalog:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating catalog';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $this->productsUpdate();
            $this->stockUpdate();
            $this->productsSetMinPrice();
        } catch (\Exception $exception) {
            Telegram::sendMessage([
                'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
                'parse_mode' => 'HTML',
                'text' => 'Обновление каталога не удалось'
            ]);
            Log::error($exception);
            return;
        }
        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'parse_mode' => 'HTML',
            'text' => 'Обновлен каталог'
        ]);
    }

    protected function stockUpdate()
    {
        set_time_limit(0);
        DB::table('availabilities')->truncate();
        $json = Storage::disk('public')->get('stock.json');
        $objs = json_decode($json, true);
        $stocks = Stock::all();
        $prevProductSku = 0;
        $product = 0;
        $chunks = array_chunk($objs, 5000);
        foreach ($chunks as $chunk) {
            $finalArray = [];
            foreach ($chunk as $obj) {
                $stock = $stocks->where('name', '=', $obj["stock_id"])->first();
                if ($stock) {
                    $obj["stock_id"] = $stock->id;
                    if ($prevProductSku != $obj["product_id"]) {
                        $product = Product::where('sku', '=', $obj["product_id"])->first();
                    }
                    $prevProductSku = $obj["product_id"];
                    if ($product) {
                        $obj["product_id"] = $product->id;
                        $finalArray[] = $obj;
                    }
                }
            }
            DB::table('availabilities')->insert($finalArray);
        }
        Log::info('Stock updated');
    }


    protected function productsUpdate()
    {
        set_time_limit(0);
        DB::table('products')->truncate();
        DB::table('categories')->truncate();
        $json = Storage::disk('public')->get('products.json');
        $objs = json_decode($json, true);

        $chunks = array_chunk($objs, 1000);

        foreach ($chunks as $chunk) {
            $finalArray = [];
            foreach ($chunk as $obj) {
                $insertArr = [];
                foreach ($obj as $key => $value) {
                    $insertArr[Str::slug($key, '_')] = $value;
                }
                $category = Category::firstOrCreate(['name' => $insertArr["category"]]);
                $insertArr["category"] = $category->id;
                $finalArray[] = $insertArr;
            }
            DB::table('products')->insert($finalArray);
        }


        Log::info('Catalog updated');
    }


    protected function productsSetMinPrice()
    {
        Product::whereHas('stocks')->chunk(2500, function ($items) {
            foreach ($items as $item) {
                $item->update([
                    'price' => $item->stocks()->min('price')
                ]);
            }
        });
        Log::info('Prices updated');
    }

}
