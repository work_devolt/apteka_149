<?php

namespace App;

use App\Enums\StatusEnum;
use Illuminate\Database\Eloquent\Model;
use Telegram\Bot\Laravel\Facades\Telegram;



class Order extends Model
{

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {


            $delivery = null;

            if ($model->pickup_options == 'Доставка' ) {
                $delivery = 'Доставка -- ' . $model->address;
            } else {
                $stock = Stock::find($model->stock_id);
                $delivery = 'Самовывоз -- ' .$stock->address;
            }

            $text = "<b>Новый заказ</b>\n"
                . "<b>Имя: </b>\n"
                . "$model->name\n"
                . "<b>Телефон: </b>\n"
                . "$model->phone\n"
                . "<b>Способ доставки: </b>\n"
                . "$delivery\n"
                . "<b>Итого: $model->total ₽</b>\n"
                . "<b>Заказ: </b>\n";




            foreach (json_decode( $model->cart, true) as $item){
                $text .= ( $item['name'] .' -- ' .$item['quantity'] .' шт. ') . $item['quantity']*$item['price'] . " ₽ \n";
            }


            Telegram::sendMessage([
                'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
                'parse_mode' => 'HTML',
                'text' => $text
            ]);


        });
    }

    protected $fillable = [
      'name',
      'phone',
      'email',
      'address',
      'cart',
      'total',
      'stock_id',
      'pickup_options',
      'status',
    ];

    public function stock() {
        return $this->belongsTo(Stock::class);
    }

}
