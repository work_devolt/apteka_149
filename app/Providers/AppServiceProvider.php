<?php

namespace App\Providers;

use App\Observers\ProductObserver;
use App\Page;
use App\Product;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\ServiceProvider;
use App\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(env('REDIRECT_HTTPS')) {
            $this->app['request']->server->set('HTTPS', true);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {

        Product::observe(ProductObserver::class);


        if(env('REDIRECT_HTTPS')) {
            $url->formatScheme('https');
        }



        Paginator::useTailwind();

        if (!$this->app->runningInConsole()) {
            $rootCategories = Category::all()->take(6);
            $pages = Page::all();
            View::share(compact('rootCategories', 'pages'));
        }
    }
}
