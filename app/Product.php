<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Stock;

class Product extends Model
{

    protected $fillable = [
        'hidden',
        'sku',
        'name',
        'description',
        'manufacturer',
        'price',
        'category',
        'qty'
    ];


    public function mainCategory()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function stocks()
    {
        return $this->belongsToMany('App\Stock', 'availabilities', 'product_id', 'stock_id')->withPivot(['qty', 'price']);
    }

    public function scopeActive($query)
    {
        return $query->where('hidden', false);
    }

    static function getExcludedArray() {
        return explode(",", setting('site.ex_products'));
    }




}
