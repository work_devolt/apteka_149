<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public function Orders () {
        return $this->hasMany( Order::class);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product', 'availabilities', 'stock_id', 'product_id')->withPivot('qty');
    }
}
