<?php

namespace App\Http\Controllers;


use App\Order;
use Darryldecode\Cart\CartCondition;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Stock;

class CheckoutController extends Controller
{

    public function index()
    {
        $stocks = Stock::all();
        return view('checkout.index', compact('stocks'));
    }


    public function post(Request $request)
    {

        $order = new Order([
            'name' => $request -> name,
            'phone' => $request-> phone,
            'address' => $request->address,
            'stock_id' => $request->stock_id,
            'pickup_options' => $request->pickup_option,
            'cart' => \Cart::getContent(),
            'total' => \Cart::getTotal(),
        ]);
        $order->save();
        \Cart::clear();

        return redirect(\URL::signedRoute('checkout.view', $order));


    }

    public function view(Order $order)
    {
        $cart = json_decode($order->cart);
        return view('checkout.view', compact('order', 'cart'));
    }

}
