<?php

namespace App\Http\Controllers;

use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class StockController extends Controller
{

    public function index() {
        $stocks = Stock::orderBy('order')->get();
        return view('contacts', compact('stocks'));
    }

    public function stockUpdate()
    {

        set_time_limit(0);
        DB::table('availabilities')->truncate();
        $json= Storage::disk('public')->get('stock.json');
//        return json_decode($json, true);
        $objs = json_decode($json,true);
        foreach ($objs as $obj)  {
            foreach ($obj as $key => $value) {
                $insertArr[Str::slug($key,'_')] = $value;
            }
            $stock = Stock::where('name', '=', $insertArr["stock_id"])->first();
            if ($stock) {
                $insertArr["stock_id"] = $stock->id;
                $product = Product::where('sku', '=', $insertArr["product_id"])->first();

                if ($product) {
                    $insertArr["product_id"] = $product -> id;
                    DB::table('availabilities')->insert($insertArr);
                }
            }
        }
        Log::info('Stock updated');
    }
}
