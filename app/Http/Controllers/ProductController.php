<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $products = Product::where('sale', true)->get();
        return view('products.show', compact('product', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    public function search(Request $request) {
        // get the search term
        $search = $request->input('search');

        // search the members table
        $products = DB::table('products')->where('name', 'ILIKE', "%{$search}%")->where('hidden', false)->get();

        // return the results
        return response()->json($products);
        }

        public function productsUpdate()
        {
            set_time_limit(0);
            DB::table('products')->truncate();
            DB::table('categories')->truncate();
            $json= Storage::disk('public')->get('products.json');
            $objs = json_decode($json,true);
            foreach ($objs as $obj)  {
                foreach ($obj as $key => $value) {
                    $insertArr[Str::slug($key,'_')] = $value;
                }
                $category = Category::firstOrCreate(['name' => $insertArr["category"]]);
                $insertArr["category"] = $category->id;
                DB::table('products')->insert($insertArr);
            }
            Log::info('Catalog updated');
        }

}
