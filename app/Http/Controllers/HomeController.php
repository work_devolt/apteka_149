<?php

namespace App\Http\Controllers;

use App\Enums\StatusEnum;
use App\Http\Requests\OrderStatusRequest;
use App\Product;
use App\Stock;
use App\Category;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Enum\Laravel\Rules\EnumRule;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $products = Product::where('sale', true)->get();
        $stocks = Stock::all();
        $categories = Category::all();
        return view('index', compact('products', 'stocks', 'categories'));
    }

    public function home()
    {
        $orders = Order::where('created_at', '>=', Carbon::yesterday())->orderByDesc('id')->paginate(30);

        return view('home', compact('orders'));
    }

    public function search(Request $request)
    {
        if($request->search != null){
            $search = $request->search;
            $orders = Order::where('id', $search)
                ->orWhere('phone', 'like', '%' . $search . '%')->get();
        }else{
            $orders = Order::all();
        }
        return view('home', compact('orders'));
    }

    public function updateStatus(OrderStatusRequest $request)
    {

        $order = Order::find($request->id);
        $order->status = $request->status;
        $order->save();

        return redirect()->route('home');
    }
}
