<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {

        return view('cart.cart');
    }

    public function add($id, Request $request)
    {
//        $shipping = new \Darryldecode\Cart\CartCondition(array(
//            'name' => 'Доставка',
//            'type' => 'shipping',
//            'target' => 'total', // this condition will be applied to cart's total when getTotal() is called.
//            'value' => + env('DELIVERY_PRICE'),
//            'order' => 1 // the order of calculation of cart base conditions. The bigger the later to be applied.
//        ));


        $product = Product::find($id);


        if ($request->quantity) {
            \Cart::remove($product->id);
            \Cart::add([
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $request->quantity,
            ]);
        } else {
            \Cart::add([
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => 1,
            ]);
        }

//        \Cart::getSubTotal() < 500 ? \Cart::condition($shipping): \Cart::clearCartConditions();

        return redirect()->back();
    }

    public function delete($id)
    {
        \Cart::remove($id);
        return redirect()->back();
    }

    public function update(Request $req)
    {
        if (!empty($req->item)) {
            foreach ($req->item as $key => $value) {
                $prev = \Cart::get($key)->quantity;
                $increment = $value - $prev;
                \Cart::update($key, array(
                    'quantity' => $increment
                ));
            }
        }
        return redirect()->back();
    }
}
